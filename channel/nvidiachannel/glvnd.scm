;;; this module contains just libglvnd stuff, no nvidia malware.
;;; hypothetically, this could be used to get libglvnd working with mesa.
(define-module (nvidiachannel glvnd)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages gl)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages xorg))

;; Some packages still need mesa for libdri
;; In those cases, build a version of mesa that supports libglvnd.
;; This prevents libGL.so and such from being inside of it, whuch would
;; conflict with the ones in libglvnd.
(define-public glvnd-mesa
  (package/inherit
   mesa
   (inputs
    (append `(("libglvnd" ,libglvnd))
            (package-inputs mesa)))
   (arguments
    (substitute-keyword-arguments
     (package-arguments mesa)
     ((#:configure-flags flags ''())
      `(cons "-Dglvnd=true" ,flags))))))

;; see the external documentation
(define-public glx-fixed-libglvnd
  (package/inherit
   libglvnd
   (source
    (origin
     (inherit (package-source libglvnd))
     (patches (search-patches "glvndpatch.patch"))))))

;; there's a problem when _replaceing_ mesa with glvd in gst-plugins-base. rpath is messed up?
;; grafting works though.
(define-public glvnd-dropin
  (package
   (name "glvd")
   (version (package-version mesa))
   (source #f)
   (build-system trivial-build-system)
   (arguments
    '(#:modules ((guix build union))
      #:builder
      (begin
        (use-modules (guix build union)
                     (srfi srfi-1)
                     (ice-9 regex))
        (union-build (assoc-ref %outputs "out")
                     (map cdr %build-inputs))
        #t)))
   (inputs
    `(
      ("libdri" ,glvnd-mesa)
      ("libglvnd" ,glx-fixed-libglvnd)
      ;; propagated by mesa, so it's not really needed, but it's good to have here explicitly
      ;; in case mesa is ever removed.
      ("libx11" ,libx11)))
   (synopsis "Drop in replacement for Mesa to use libglvnd.")
   (description "This package can be substituted for Mesa to make the resulting program use libglvnd.")
   (home-page "https://gitlab.com/nonguix/nonguix")
   (license
    (cons (package-license mesa) (package-license libglvnd)))))

(define-public mesa-graft-to-glvnd-package
  (package
   (inherit mesa)
   (replacement glvnd-dropin)))
(define-public glvnd-transformations-list-graft
  `((,mesa . ,mesa-graft-to-glvnd-package)))
(define-public glvnd-transformation-graft
  (package-input-rewriting glvnd-transformations-list-graft))
