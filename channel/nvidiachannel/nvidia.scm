;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Hebi Li <hebi@lihebi.com>
;;; Copyright © 2020 Malte Frank Gerdes <malte.f.gerdes@gmail.com>
;;; Copyright © 2020, 2021 Jean-Baptiste Volatier <jbv@pm.me>
;;; Copyright © 2020, 2021 Jonathan Brielmaier <jonathan.brielmaier@web.de>
;;; Copyright © 2021 Pierre Langlois <pierre.langlois@gmx.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (nvidiachannel nvidia)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module (gnu packages video)
  #:use-module (guix utils)
  #:use-module ((nonguix licenses) #:prefix license:)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages m4)
  #:use-module (guix build-system linux-module)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages graphics)
  #:use-module (nongnu packages linux)
  #:use-module (nvidiachannel glvnd)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 format)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))

(define (norigin version sha)
  (origin
   (uri (format #f "http://us.download.nvidia.com/XFree86/Linux-x86_64/~a/~a.run"
                version
                (format #f "NVIDIA-Linux-x86_64-~a" version)))
   (sha256 sha)
   (method url-fetch)
   (file-name (string-append "nvidia-driver-" version "-checkout"))))
(define (nunpack version)
  `(replace
    'unpack
    (lambda* (#:key inputs #:allow-other-keys #:rest r)
      (let ((source (assoc-ref inputs "source")))
        (invoke "sh" source "--extract-only")
        (chdir ,(format #f "NVIDIA-Linux-x86_64-~a" version))
        #t))))

;; inputs used just to extract the driver
(define nextraction-inputs
  `(("which" ,which)
    ("xz" ,xz)
    ("bash-minimal" ,bash-minimal)
    ("coreutils" ,coreutils)
    ("grep" ,grep)))

(define-public (gen-nvidia-module version sha)
  (package
   (name "nvidia-module")
   (version version)
   (source (norigin version sha))
   (build-system linux-module-build-system)
   (arguments
    `(#:linux ,linux
      #:source-directory "kernel"
      #:phases
      (modify-phases %standard-phases
                     ,(nunpack version)
                     (replace
                      'build
                      (lambda*  (#:key inputs outputs #:allow-other-keys)
                        (with-directory-excursion
                         "kernel"
                         ;; Patch Kbuild
                         (substitute* "Kbuild"
                                      (("/bin/sh") (string-append (assoc-ref inputs "bash-minimal") "/bin/sh")))
                         (invoke "make"
                                 "-j"
                                 (string-append "SYSSRC="
                                                (assoc-ref inputs "linux-module-builder")
                                                "/lib/modules/build")
                                 "CC=gcc"))))
                     (delete 'check))))
   (native-inputs nextraction-inputs)
   (home-page "kernel")
   (synopsis "kernel")
   (description "kernel")
   (license (license:nonfree (format #f "file:///share/doc/nvidia-driver-~a/LICENSE" version)))))

(define-public nvidia-udev
  (package
   (name "nvidia-udev")
   (version "001")
   (source #f)
   (build-system trivial-build-system)
   (arguments
    '(#:modules ((ice-9 textual-ports) (guix build utils))
      #:builder
      (begin
        (use-modules (ice-9 textual-ports) (guix build utils))
        (let* ((uout (assoc-ref %outputs "out"))
               (rulesdir (string-append uout "/lib/udev/rules.d/"))
               (rules    (string-append uout "/lib/udev/rules.d/90-nvidia.rules"))
               (sh       (string-append (assoc-ref %build-inputs "bash-minimal") "/bin/sh"))
               (mknod    (string-append (assoc-ref %build-inputs "coreutils") "/bin/mknod"))
               (cut     (string-append (assoc-ref %build-inputs "coreutils") "/bin/cut"))
               (grep     (string-append (assoc-ref %build-inputs "grep") "/bin/grep")))
          (mkdir-p rulesdir)
          (call-with-output-file rules
            (lambda (port)
              (put-string port
                          (string-append
                           "KERNEL==\"nvidia\", "
                           "RUN+=\"" sh " -c '" mknod " -m 666 /dev/nvidiactl c $$(" grep " nvidia-frontend /proc/devices | " cut " -d \\  -f 1) 255'\"" "\n"
                           "KERNEL==\"nvidia_modeset\", "
                           "RUN+=\"" sh " -c '" mknod " -m 666 /dev/nvidia-modeset c $$(" grep " nvidia-frontend /proc/devices | " cut " -d \\  -f 1) 254'\"" "\n"
                           "KERNEL==\"card*\", SUBSYSTEM==\"drm\", DRIVERS==\"nvidia\", "
                           "RUN+=\"" sh " -c '" mknod " -m 666 /dev/nvidia0 c $$(" grep " nvidia-frontend /proc/devices | " cut " -d \\  -f 1) 0'\"" "\n"
                           "KERNEL==\"nvidia_uvm\", "
                           "RUN+=\"" sh " -c '" mknod " -m 666 /dev/nvidia-uvm c $$(" grep " nvidia-uvm /proc/devices | " cut " -d \\  -f 1) 0'\"" "\n"
                           "KERNEL==\"nvidia_uvm\", "
                           "RUN+=\"" sh " -c '" mknod " -m 666 /dev/nvidia-uvm-tools c $$(" grep " nvidia-uvm /proc/devices | " cut " -d \\  -f 1) 0'\"" "\n" )))))
        )))
   (native-inputs
    `(
      ("bash-minimal" ,bash-minimal)
      ("coreutils" ,coreutils)
      ("grep" ,grep)))
   (home-page "https://gitlab.com/nonguix/nonguix")
   (synopsis "udev")
   (description "nvidia udev rules i took from the old package definition, which look similar to ones i saw in nix. may or may not actually be needed.")
   ;; license for this package definition
   (license license:gpl3+)))

(define-public (gen-nvidia-libs-minimal version sha)
  (package
   (name "nvidia-libs-minimal")
   (version version)
   (source (norigin version sha))
   (build-system gnu-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     ,(nunpack version)
                     (replace
                      'install
                      (lambda* (#:key inputs native-inputs propagated-inputs outputs #:allow-other-keys)
                        (use-modules (ice-9 ftw)
                                     (ice-9 regex)
                                     (ice-9 textual-ports)
                                     (ice-9 rdelim)
                                     (ice-9 popen)
                                     (srfi srfi-1)
                                     (guix build utils))
                        (define (install-patched-json srcfile destpath libpath)
                          (mkdir-p destpath)
                          (let ((inport (open-input-file srcfile)))
                            (call-with-output-file (string-append destpath "/" (basename srcfile))
                              (lambda (outport)
                                (do ((line (read-line inport) (read-line inport)))
                                    ((eof-object? line))
                                  (when (string-contains line "library_path")
                                    (let ((libpath
                                           (canonicalize-path
                                            (string-append
                                             libpath
                                             "/"
                                             (regexp-substitute
                                              #f
                                              (string-match "\\s*\"library_path\"\\s*:\\s*\"(.*?)\"" line)
                                              1)))))
                                      (set! line
                                            (string-append "\"library_path\" : \"" libpath "\"" (if (string-suffix? "," line) "," "")))))
                                  (display line outport)
                                  (newline outport))))
                            (close-port inport)))
                        (let* ((out (assoc-ref outputs "out"))
                               (libdir (string-append out "/lib"))
                               (bindir (string-append out "/bin"))
                               (etcdir (string-append out "/etc"))
                               (sharedir (string-append out "/share")))

                          (install-file "nvidia-smi" bindir)

                          ;; skipping something arch installs: some man pages.
                          ;; skipping something arch installs: 10-nvidia-drm-outputclass.conf is made by arch developers.

                          ;; executable files arch installs:
                          ;; [ ] ./usr/bin/nvidia-sleep.sh
                          ;; [ ] ./usr/bin/nvidia-xconfig
                          ;;   https://download.nvidia.com/XFree86/nvidia-xconfig/.
                          ;; [ ] ./usr/bin/nvidia-cuda-mps-server
                          ;; [ ] ./usr/bin/nvidia-debugdump
                          ;; [ ] ./usr/bin/nvidia-modprobe
                          ;;   https://download.nvidia.com/XFree86/nvidia-modprobe/
                          ;; [ ] ./usr/bin/nvidia-cuda-mps-control
                          ;; [X] ./usr/bin/nvidia-smi 
                          ;; [ ] ./usr/bin/nvidia-persistenced
                          ;;   https://download.nvidia.com/XFree86/nvidia-persistenced/
                          ;; [ ] ./usr/bin/nvidia-ngx-updater
                          ;; [ ] ./usr/bin/nvidia-bug-report.sh

                          ;; Copy .so files
                          (let ((desired-so-files
                                 (scandir
                                  "."
                                  (lambda (name)
                                    (and 
                                     (string-contains name ".so")
                                     (not
                                      (or
                                       (any (lambda (a) (string-prefix? a name))
                                            `(
                                              ;; libraries provided by libglvnd
                                              "libEGL.so."
                                              "libGL.so"
                                              "libGLdispatch.so."
                                              "libGLESv1_CM.so."
                                              "libGLESv2.so."
                                              "libGLX.so."
                                              "libOpenGL.so."

                                              ;; libraries that are installed elsewhere
                                              "libglxserver_nvidia.so."
                                              "nvidia_drv.so"

                                              ;; libraries we can compile ourselves:
                                              "libnvidia-egl-wayland.so."
                                              ;; according to the documentation, only used by nvidia-settings. useless:
                                              "libnvidia-gtk2.so."
                                              "libnvidia-gtk3.so."

                                              ;; these may actually be important.
                                              "libnvidia-fbc.so." ;; depends on libGL.
                                              "libOpenCL.so." ;; who cares?
                                              )))))))))
                            (for-each
                             (lambda (file)
                               (format #t "Copying '~a'...~%" file)
                               (install-file file libdir))
                             desired-so-files)

                            (install-file "nvidia_drv.so" (string-append out "/lib/xorg/modules/drivers/"))
                            (install-file ,(string-append "libglxserver_nvidia.so." version) (string-append out "/lib/xorg/modules/extensions/"))

                            (let* ((libc (assoc-ref inputs "libc"))
                                   (ld.so (string-append libc ,(glibc-dynamic-linker)))
                                   (rpath (string-join
                                           (list "$ORIGIN"
                                                 libdir
                                                 (string-append libc "/lib")

                                                 (string-append (assoc-ref inputs "wayland") "/lib")
                                                 (string-append (assoc-ref inputs "libx11") "/lib")
                                                 (string-append (assoc-ref inputs "libxext") "/lib")
                                                 (string-append (assoc-ref inputs "gcc:lib") "/lib")

                                                 (string-append (assoc-ref inputs "libdrm") "/lib")
                                                 (string-append (assoc-ref inputs "libglvnd") "/lib")
                                                 (string-append (assoc-ref inputs "mesa") "/lib")

                                                 (string-append (assoc-ref %build-inputs "egl-wayland") "/lib"))
                                           ":")))
                              (define (patch-elf file)
                                (format #t "Patching ~a ...~%" file)
                                (unless (string-contains file ".so")
                                  (invoke "patchelf" "--set-interpreter" ld.so file))
                                (invoke "patchelf" "--set-rpath" rpath file))
                              (define (maybepatch file)
                                (when (elf-file? file)
                                  (patch-elf file)))
                              (for-each maybepatch (find-files out  ".*\\.so"))
                              (patch-elf (string-append bindir "/" "nvidia-smi")))

                            ;; make shortname symlinks
                            (let ((readelf (string-append (assoc-ref inputs "binutils") "/bin/readelf")))
                              (for-each
                               (lambda (file)
                                 (let ((bname (basename file)))
                                   (let ((port (open-pipe* OPEN_READ readelf "-d" file)))
                                     (while 't
                                       (let ((str (read-line port)))
                                         (when (eof-object? str)
                                           (format (current-error-port) "Not making symlink for: '~a'~%" file)
                                           (break))
                                         (when (string-contains str "(SONAME)")
                                           (let* ((left (string-index str #\[))
                                                  (right (string-rindex str #\]))
                                                  (soname (substring str (1+ left) right)))
                                             (when (not (string= bname soname))
                                               (format #t "Link '~a' to '~a'...~%" file soname)
                                               (symlink bname (string-append libdir "/" soname)))
                                             (break)))))
                                     (close-pipe port))
                                   (let* ((shortestname (substring bname 0 (+ (string-contains bname ".so") 3)))
                                          (linkpath (string-append libdir "/" shortestname)))
                                     (when (not (file-exists? linkpath))
                                       (format #t "shortestname: ~s\n" shortestname)
                                       (symlink bname linkpath)))
                                   ))
                               desired-so-files)))
                          (symlink ,(string-append "libglxserver_nvidia.so." version) (string-append out "/lib/xorg/modules/extensions/" "libglxserver_nvidia.so"))

                          ;; the nvidia libraries are hard coded to search for these in the standard locations
                          ;; i checked the nvidia documentation and i dont see an environment variable to change the location.
                          ;; right now these do nothing.
                          ;; if we want them to work, we have options:
                          ;; (1) read the documentation again and find that there actually is a way to override the path
                          ;; (2) make users copy to their home directories
                          ;; (3) add an etc-service-type
                          (let ((nvshare (string-append sharedir "/nvidia/")))
                            (install-file (string-append "nvidia-application-profiles-" ,version "-key-documentation") nvshare)
                            (install-file (string-append "nvidia-application-profiles-" ,version "-rc") nvshare))
                          
                          ;; install json files for glvnd
                          (install-patched-json "10_nvidia.json" (string-append sharedir "/glvnd/egl_vendor.d/") libdir)

                          (let ((eglexternaldir (string-append sharedir "/egl/egl_external_platform.d/")))
                            (install-patched-json "15_nvidia_gbm.json" eglexternaldir libdir)
                            (install-patched-json "10_nvidia_wayland.json" eglexternaldir (string-append (assoc-ref inputs "egl-wayland") "/lib")))

                          (let ((vdir (string-append sharedir "/vulkan/")))
                            (install-patched-json "nvidia_layers.json" (string-append vdir "/implicit_layer.d") libdir)
                            (install-patched-json "nvidia_icd.json" (string-append vdir "/icd.d") libdir)))))
                     (delete 'configure)
                     (delete 'build)
                     (delete 'check)
                     ;; required, or you get "nvidia-smi: symbol lookup error: nvidia-smi: undefined symbol: , version GLIBC_2.2.5"
                     (delete 'strip))))
   (native-search-paths
    (list (search-path-specification
           (variable "__EGL_VENDOR_LIBRARY_DIRS")
           (files '("share/glvnd/egl_vendor.d")))
          (search-path-specification
           (variable "__EGL_EXTERNAL_PLATFORM_CONFIG_FILENAMES")
           (files '("share/egl/egl_external_platform.d/15_nvidia_gbm.json"))
           (file-type 'regular))
          (search-path-specification
           (variable "VK_ICD_FILENAMES")
           (files '("share/vulkan/icd.d/nvidia_icd.json"))
           (file-type 'regular))
          (search-path-specification
           (variable "VK_LAYER_PATH")
           (files '("share/vulkan/implicit_layer.d")))
          (search-path-specification
           (variable "VDPAU_DRIVER_PATH")
           (files '("lib")))
          ;; for my made-up libglvnd mod
          (search-path-specification
           (variable "GUIX_GLVND_GLXLIB")
           (files '("lib/libGLX_nvidia.so.0"))
           (file-type 'regular))
          ;; it would be nice if these could be set too:
          ;; VDPAU_DRIVER=nvidia
          ;; __GLX_VENDOR_LIBRARY_NAME=nvidia
          ))
   (native-inputs
    (append `(("patchelf" ,patchelf))
            nextraction-inputs))
   (inputs
    `(("binutils" ,binutils)
      ("kmod" ,kmod)
      ("linux" ,linux)

      ;; libGLX_nvidia, libvdpau_nvidia:
      ("libx11" ,libx11)
      ("libxext" ,libxext)

      ;; libnvidia-egl-wayland
      ("wayland" ,wayland)

      ;; libnvidia-rtcore.so
      ("gcc:lib" ,gcc "lib")

      ;; 10_nvidia_wayland.json
      ("egl-wayland" ,egl-wayland)
      
      ;; had to add these to get libnvidia-egl-gbm working.
      ;; these are already dependencies of mesa with libglvnd, so whatever.
      ("libdrm" ,libdrm)
      ("libglvnd" ,libglvnd)
      ("mesa" ,glvnd-mesa)
      ))
   (home-page "libs")
   (synopsis "libs")
   (description "malware libraries for nvidia driver.")
   (license (license:nonfree (format #f "file:///share/doc/nvidia-driver-~a/LICENSE" version)))))

(define-public (gen-nvidia-settings version sha)
  (package
   (name "nvidia-settings")
   (version version)
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/NVIDIA/nvidia-settings")
                  (commit version)))
            (sha256
             (base32
              sha))))
   (build-system gnu-build-system)
   (arguments
    `(#:make-flags (list (string-append "CC=" ,(cc-for-target))
                         (string-append "PREFIX=" %output))
      #:phases
      (modify-phases %standard-phases
                     (delete 'configure)
                     (delete 'check)
                     ;; nvidia-settings looks for its libraries, libnvidia-gtk{2,3}.so at runtime.
                     ;; make sure they're available
                     (add-after 'install 'wrap-program
                                (lambda* (#:key outputs #:allow-other-keys)
                                  (let ((out (assoc-ref outputs "out")))
                                    (wrap-program (string-append out "/bin/nvidia-settings")
                                                  `("LD_LIBRARY_PATH" ":" prefix (,(string-append out "/lib/"))))))))))

   (native-inputs `(("pkg-config", pkg-config)
                    ("m4" ,m4)))
   (inputs `(("libxv" ,libxv)
             ("gtk+" ,gtk+)
             ("dbus" ,dbus)
             ("gtk2" ,gtk+-2)
             ("libvdpau" ,libvdpau)
             ("libxext" ,libxext)
             ("libxxf86vm" ,libxxf86vm)
             ("libx11" ,libx11)
             ("glu" ,glu)
             ("libxrandr" ,libxrandr)))    
   (synopsis "NVIDIA driver control panel")
   (description "This is the NVIDIA driver control panel for monitor configuration, creating application profiles, gpu monitoring, etc.")
   (home-page "https://github.com/NVIDIA/nvidia-settings")
   (license license:gpl2)))

(define nversion "495.46")
(define nsha (base32 "1xb23kimlfsailpwy7kv4l217id5jpmdc4svm9kldid0gp8pffyq"))

(define-public nvidia-settings (gen-nvidia-settings nversion "1yb4n6x8z8y9hyw74wcd2wn8jzxpjwnfkjgsinq1214lgxi1kdxx"))
(define-public nvidia-libs-minimal (gen-nvidia-libs-minimal nversion nsha))
(define-public nvidia-module (gen-nvidia-module nversion nsha))

