(load "./gtransform.scm")
(packages->manifest
 (fixpkgs
  '("mpv" ;; mp4 viewer
    "imv" ;; jpeg viewer that uses the gpu
    "mesa-utils" ;; for `glxgears' to see if glx is working.
    "vulkan-tools" ;; for `vulkaninfo' to see if vulkan is working
    ("font-adobe-source-han-sans" "jp") ;; important for chinese cartoons
    ;; This next one is how you would exclude a package from being transformed.
    ;; But something is going on with the substitute servers right now, so
    ;; this would compile ungoogled-chromium anyway.
    ;;(raw "ungoogled-chromium")
    )))
