
;; Warning:
;; This will not work without modification, obviously.
;; Read through and see where modifications are needed.
;; Also, it doesn't include wpa-supplicant.
;; So if you don't have a wired connection, add it.

(use-modules (gnu))
(use-modules (nongnu packages linux) (nongnu system linux-initrd))
(use-modules (srfi srfi-1)) ;; "remove"
(use-service-modules xorg
                     linux
                     desktop ;; %desktop-services
                     networking ;; modem-manager-service-type
                     )
(use-package-modules xorg wm certs
                     display-managers
                     freedesktop ;; libinput
                     suckless ;; st
                     )

;; make sure you downloaded this file too
(load "./gtransform.scm")

;; special xorg config.
;; adds nvidia xorg module and transforms xorg-server package
(define my-xorg-conf
  (xorg-configuration
   (modules
    (cons*
     (fixpkg nvidia-libs-minimal)
     ;; optional: remove garbage.
     (remove
      (lambda (pkg)
        (member pkg
                (list
                 xf86-video-amdgpu
                 xf86-video-ati
                 xf86-video-cirrus
                 xf86-video-intel
                 xf86-video-mach64
                 xf86-video-nouveau
                 xf86-video-nv
                 xf86-video-sis)))
      %default-xorg-modules)))
   (server (fixpkg xorg-server))
   (drivers '("nvidia"))))

(operating-system
 (host-name "nvidiatest")
 (timezone "America/New_York")
 (locale "en_US.utf8")

 (initrd microcode-initrd)
 (initrd-modules %base-initrd-modules)

 (kernel (fixpkg linux))
 (kernel-loadable-modules (list (fixpkg nvidia-module)))
 (kernel-arguments (list
                    ;; enable a feature
                    "nvidia-drm.modeset=1"
                    ;; nvidia_uvm gives me problems
                    "modprobe.blacklist=nouveau,nvidia_uvm"))
 (firmware (fixpkgs (list linux-firmware)))


 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;; BOOTLOADER CONFIGURATION GOES HERE
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;; FILE-SYSTEMS CONFIGURATION GOES HERE
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;
 ;;

 (users (cons (user-account
               (name "user")
               (group "users")
               ;; default password is "a"
               (password (crypt "a" "$6$abc"))
               (supplementary-groups '("wheel" "audio" "video")))
              %base-user-accounts))

 (packages
  (fixpkgs
   (cons*
    nss-certs

    ;; does this actually have to be global? i don't know. i do it anyway.
    nvidia-libs-minimal

    ;; are these even needed? i don't remember.
    xf86-input-libinput
    libinput

    ;; packages so you can actually test to see if it works
    i3-gaps ;; window manager
    st

    %base-packages)))

 (services
  (cons*
   ;; nvidia udev service
   (simple-service
    'my-nvidia-udev-rules udev-service-type
    (list (fixpkg nvidia-udev)))

   ;; add liglvnd slim using special xorg config
   (service slim-service-type
            (slim-configuration
             (slim (fixpkg slim))
             (xorg-configuration my-xorg-conf)))

   (service kernel-module-loader-service-type
            '("nvidia"
              "nvidia_modeset"
              ;; i dont remember why i put this one here.
              ;; i think i stole it from somebody else.
              ;; maybe it's not needed.
              "ipmi_devintf"))

   (service dhcp-client-service-type)
   
   ;; default services, with some packages removed for faster build
   (remove (lambda (service)
             (or (member (service-kind service)
                         (list
                          gdm-service-type
                          modem-manager-service-type
                          usb-modeswitch-service-type
                          wpa-supplicant-service-type
                          network-manager-service-type
                          geoclue-service-type
                          colord-service-type))
                 (member
                  (struct-ref (service-kind service) 0)
                  '(
                    screen-locker
                    mount-setuid-helpers
                    network-manager-applet
                    ))))
           %desktop-services))))
